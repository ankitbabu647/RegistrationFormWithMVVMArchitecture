package com.example.registrationformapplication.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.registrationformapplication.R
import com.example.registrationformapplication.databinding.ActivityLoginBinding
import com.example.registrationformapplication.factory.SignUpSharedPrefRepositoryViewModalFactory
import com.example.registrationformapplication.repository.SignUpSharedPrefRepository
import com.example.registrationformapplication.utility.Keys
import com.example.registrationformapplication.viewModal.SignUpSharedPrefRepositoryViewModel

class LoginActivity : AppCompatActivity(), View.OnClickListener{
    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: SignUpSharedPrefRepositoryViewModel
    private lateinit var factory: SignUpSharedPrefRepositoryViewModalFactory
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_login)
        factory= SignUpSharedPrefRepositoryViewModalFactory(SignUpSharedPrefRepository,this)
        viewModel=ViewModelProvider(this,factory)[SignUpSharedPrefRepositoryViewModel::class.java]



        binding.btnLogin.setOnClickListener(this)
        binding.tvSkiplogin.setOnClickListener(this)
        binding.tvDonthaveaccount.setOnClickListener(this)

        binding.mylogindata=viewModel

        binding.lifecycleOwner=this
    }

    override fun onClick(view: View?) {
        when(view?.id){
            R.id.btn_login->{
                    if (viewModel.getData(binding.edPhonenumber.editText?.text.toString())){
                            var intent = Intent(this,FormActivity::class.java)
                            val usertypedate : String? = intent.getStringExtra(Keys.USER_TYPE)
                            intent=intent.putExtra( Keys.USER_TYPE,usertypedate)
                            startActivity(intent)
                        }

            }


            R.id.tv_skiplogin->{
                val intent= Intent(this,FormActivity::class.java)
                intent.putExtra(Keys.USER_TYPE,"Guest")
                startActivity(intent)
            }


            R.id.tv_donthaveaccount->{
              val intent=Intent(this,SignUpActivity::class.java)
              startActivity(intent)
            }

        }

    }

}